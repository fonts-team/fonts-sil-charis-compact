Source: fonts-sil-charis-compact
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Hideki Yamane <henrich@debian.org>,
Build-Depends: debhelper-compat (= 13),
Standards-Version: 4.6.2
Homepage: https://software.sil.org/charis/
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-charis-compact.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-charis-compact
Rules-Requires-Root: no

Package: fonts-sil-charis-compact
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Suggests: libgraphite2-3
Description: smart Unicode fonts for Latin and Cyrillic scripts (tight spacing version)
 The goal for Charis SIL is to provide a single Unicode-based font family
 that would contain a comprehensive inventory of glyphs needed for almost
 any Roman- or Cyrillic-based writing system, whether used for phonetic or
 orthographic needs. In addition, there is provision for other characters
 and symbols useful to linguists.
 .
 The Charis SIL font contains near-complete coverage of all the characters
 defined in Unicode 7.0 for Latin and Cyrillic. In total, over 3,600 glyphs are
 included, providing support for over 2,300 characters as well as a large number
 of ligated character sequences (e.g., contour tone letters used in phonetic
 transcription of tonal languages). 
 .
 Four fonts from this typeface family are included in this release:
    * Charis SIL Compact Regular
    * Charis SIL Compact Bold
    * Charis SIL Compact Italic
    * Charis SIL Compact Bold Italic
 .
 Charis SIL is a TrueType font with "smart font" capabilities added using the
 Graphite, OpenType(r), and AAT font technologies. This means that complex
 typographic issues such as formation of ligatures or automatic positioning of
 diacritics relative to base characters in arbitrary base+diacritic combinations
 (including combinations involving multiple diacritics) are handled by the font.
 In addition, alternately-designed glyphs are also provided for a number of
 characters for use in particular contexts. These capabilities are only
 available if you are running an application that provides an adequate level of
 support for one of these smart font technologies.
 .
 This is the compact version with tighter line spacing.

